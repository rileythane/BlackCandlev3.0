﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageManager : MonoBehaviour
{

    public GameObject bourdain, pigs, homeless, pills, pollution, politics, consumers;
    public float clickCount;
    public AudioTimer at;
    Animator anim;


    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    IEnumerator PageWait()
    {
        yield return new WaitForSeconds(0.5f);
        ImageSelector();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0) && at.windBlowing == false)
        {
            anim.SetTrigger("Active");
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && at.windBlowing == true)
        {
            clickCount++;
            StartCoroutine(PageWait());
            anim.SetTrigger("Active");
        }
    }

    void ImageSelector()
    {
        if (clickCount == 1)
        {
            pigs.SetActive(true);
            homeless.SetActive(false);
            bourdain.SetActive(false);
            pills.SetActive(false);
            pollution.SetActive(false);
            consumers.SetActive(false);
            politics.SetActive(false);
        }

        if (clickCount == 2)
        {
            pigs.SetActive(false);
            homeless.SetActive(true);
            bourdain.SetActive(false);
            pills.SetActive(false);
            pollution.SetActive(false);
            consumers.SetActive(false);
            politics.SetActive(false);
        }

        if (clickCount == 3)
        {
            pigs.SetActive(false);
            homeless.SetActive(false);
            bourdain.SetActive(true);
            pills.SetActive(false);
            pollution.SetActive(false);
            consumers.SetActive(false);
            politics.SetActive(false);
        }

        if (clickCount == 4)
        {
            pigs.SetActive(false);
            homeless.SetActive(false);
            bourdain.SetActive(false);
            pills.SetActive(true);
            pollution.SetActive(false);
            consumers.SetActive(false);
            politics.SetActive(false);
        }

        if (clickCount == 5)
        {
            pigs.SetActive(false);
            homeless.SetActive(false);
            bourdain.SetActive(false);
            pills.SetActive(false);
            pollution.SetActive(true);
            consumers.SetActive(false);
            politics.SetActive(false);
        }

        if (clickCount == 6)
        {
            pigs.SetActive(false);
            homeless.SetActive(false);
            bourdain.SetActive(false);
            pills.SetActive(false);
            pollution.SetActive(false);
            consumers.SetActive(true);
            politics.SetActive(false);
        }

        if (clickCount == 7)
        {
            pigs.SetActive(false);
            homeless.SetActive(false);
            bourdain.SetActive(false);
            pills.SetActive(false);
            pollution.SetActive(false);
            consumers.SetActive(false);
            politics.SetActive(true);
        }


        if (clickCount > 7)
        {
            clickCount = 0;
        }
    }
}
