﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTimer : MonoBehaviour {

    public AudioSource source;
    public AudioClip windClip, pageTurn, slamClip;
    public GameObject bookLight, candleLight;

    public float firstWait, secondWait;
    public bool windBlowing;


    // Use this for initialization
    void Start()
    {
        candleLight.SetActive(true);
        bookLight.SetActive(false);
        windBlowing = false;
        StartCoroutine(WindWait());
    }

    IEnumerator WindWait()
    {
        yield return new WaitForSeconds(firstWait);
        source.PlayOneShot(windClip);
        StartCoroutine(SecondWindWait());
    }

    IEnumerator SecondWindWait()
    {
        yield return new WaitForSeconds(secondWait);
        source.PlayOneShot(windClip);

        yield return new WaitForSeconds(10);
        source.PlayOneShot(slamClip);
        windBlowing = true;
        bookLight.SetActive(true);
        candleLight.SetActive(false);

    }
	
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            print("click");
            source.PlayOneShot(pageTurn);
        }
    }
}
